EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ESWINO"
Date "2021-03-06"
Rev "V100"
Comp "Unicersidad de Cordoba"
Comment1 "TFM Francisco Manuel Alvarez Wic"
Comment2 "Development status"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1230 6250 1230 6150
Wire Wire Line
	1730 6550 1830 6550
Wire Wire Line
	1730 6750 1830 6750
Wire Wire Line
	1730 6850 1830 6850
Wire Wire Line
	1730 6950 1830 6950
Wire Wire Line
	1730 7050 1830 7050
Wire Wire Line
	1130 7450 1130 7550
Wire Wire Line
	1230 7450 1230 7550
NoConn ~ 1130 7550
NoConn ~ 1830 6950
NoConn ~ 1830 7050
$Comp
L power:GND #PWR?
U 1 1 604C4080
P 1230 7550
AR Path="/5EE620E3/604C4080" Ref="#PWR?"  Part="1" 
AR Path="/604C4080" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1230 7300 50  0001 C CNN
F 1 "GND" H 1330 7400 50  0000 C CNN
F 2 "" H 1230 7550 50  0001 C CNN
F 3 "" H 1230 7550 50  0001 C CNN
	1    1230 7550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_ARM_JTAG_SWD_10 J?
U 1 1 604C4130
P 1230 6850
AR Path="/5EE620E3/604C4130" Ref="J?"  Part="1" 
AR Path="/604C4130" Ref="J1"  Part="1" 
F 0 "J1" H 787 6896 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" V 630 7350 50  0000 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_2x05_P2.00mm_Vertical" H 1230 6850 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 880 5600 50  0001 C CNN
	1    1230 6850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 604C414E
P 1230 6150
AR Path="/5EE620E3/604C414E" Ref="#PWR?"  Part="1" 
AR Path="/604C414E" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1230 6000 50  0001 C CNN
F 1 "+3.3V" H 1245 6323 50  0000 C CNN
F 2 "" H 1230 6150 50  0001 C CNN
F 3 "" H 1230 6150 50  0001 C CNN
	1    1230 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3340 6010 3090 6010
Wire Wire Line
	3340 6110 3090 6110
Wire Wire Line
	3090 6210 3340 6210
Wire Wire Line
	2590 6010 2340 6010
Wire Wire Line
	2590 6110 2340 6110
Wire Wire Line
	2590 6210 2340 6210
Wire Wire Line
	2340 6310 2590 6310
Text Label 2340 6010 0    50   ~ 0
AREF
Text Label 3340 6010 2    50   ~ 0
A0
Text Label 2340 6110 0    50   ~ 0
A1
Text Label 3340 6110 2    50   ~ 0
A2
Text Label 2340 6210 0    50   ~ 0
A3
Text Label 3340 6210 2    50   ~ 0
A4
Text Label 2340 6310 0    50   ~ 0
A5
Wire Wire Line
	3090 6310 3340 6310
Text Label 3340 6310 2    50   ~ 0
A6
Wire Wire Line
	3750 5980 4000 5980
Wire Wire Line
	3750 6080 4000 6080
Wire Wire Line
	3750 6180 4000 6180
Wire Wire Line
	3750 6280 4000 6280
Wire Wire Line
	4500 6380 4750 6380
Wire Wire Line
	4500 6280 4750 6280
Wire Wire Line
	4500 6180 4750 6180
Wire Wire Line
	4500 6080 4750 6080
Wire Wire Line
	4500 5980 4750 5980
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 604C41F2
P 4200 6180
AR Path="/5EE620E3/604C41F2" Ref="J?"  Part="1" 
AR Path="/604C41F2" Ref="J5"  Part="1" 
F 0 "J5" H 4250 6597 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 4250 6506 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4200 6180 50  0001 C CNN
F 3 "~" H 4200 6180 50  0001 C CNN
	1    4200 6180
	1    0    0    -1  
$EndComp
Text Label 3750 5980 0    50   ~ 0
W0
Text Label 4750 5980 2    50   ~ 0
W1
Text Label 3750 6080 0    50   ~ 0
TX_0
Text Label 4750 6080 2    50   ~ 0
RX_0
Text Label 3750 6180 0    50   ~ 0
W4
Text Label 4750 6180 2    50   ~ 0
W5
Text Label 3750 6280 0    50   ~ 0
W6
Text Label 4750 6280 2    50   ~ 0
W7
$Comp
L power:+3.3V #PWR?
U 1 1 604C4200
P 2190 5960
AR Path="/5EE620E3/604C4200" Ref="#PWR?"  Part="1" 
AR Path="/604C4200" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2190 5810 50  0001 C CNN
F 1 "+3.3V" H 2205 6133 50  0000 C CNN
F 2 "" H 2190 5960 50  0001 C CNN
F 3 "" H 2190 5960 50  0001 C CNN
	1    2190 5960
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5930 3600 6380
Wire Wire Line
	3600 6380 4000 6380
$Comp
L power:GND #PWR?
U 1 1 604C4208
P 4750 6430
AR Path="/5EE620E3/604C4208" Ref="#PWR?"  Part="1" 
AR Path="/604C4208" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4750 6180 50  0001 C CNN
F 1 "GND" H 4850 6280 50  0000 C CNN
F 2 "" H 4750 6430 50  0001 C CNN
F 3 "" H 4750 6430 50  0001 C CNN
	1    4750 6430
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6380 4750 6430
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 604C420F
P 5930 6290
AR Path="/5EE620E3/604C420F" Ref="J?"  Part="1" 
AR Path="/604C420F" Ref="J7"  Part="1" 
F 0 "J7" H 5980 6707 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5980 6616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5930 6290 50  0001 C CNN
F 3 "~" H 5930 6290 50  0001 C CNN
	1    5930 6290
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 604C4215
P 2790 6210
AR Path="/5EE620E3/604C4215" Ref="J?"  Part="1" 
AR Path="/604C4215" Ref="J2"  Part="1" 
F 0 "J2" H 2840 6627 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2840 6536 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2790 6210 50  0001 C CNN
F 3 "~" H 2790 6210 50  0001 C CNN
	1    2790 6210
	1    0    0    -1  
$EndComp
Wire Wire Line
	2190 5960 2190 6410
Wire Wire Line
	2190 6410 2590 6410
Wire Wire Line
	3090 6410 3340 6410
$Comp
L power:GND #PWR?
U 1 1 604C421E
P 3340 6460
AR Path="/5EE620E3/604C421E" Ref="#PWR?"  Part="1" 
AR Path="/604C421E" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 3340 6210 50  0001 C CNN
F 1 "GND" H 3440 6310 50  0000 C CNN
F 2 "" H 3340 6460 50  0001 C CNN
F 3 "" H 3340 6460 50  0001 C CNN
	1    3340 6460
	1    0    0    -1  
$EndComp
Wire Wire Line
	3340 6410 3340 6460
Wire Wire Line
	6230 6490 6480 6490
$Comp
L power:GND #PWR?
U 1 1 604C4226
P 6480 6540
AR Path="/5EE620E3/604C4226" Ref="#PWR?"  Part="1" 
AR Path="/604C4226" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 6480 6290 50  0001 C CNN
F 1 "GND" H 6580 6390 50  0000 C CNN
F 2 "" H 6480 6540 50  0001 C CNN
F 3 "" H 6480 6540 50  0001 C CNN
	1    6480 6540
	1    0    0    -1  
$EndComp
Wire Wire Line
	6480 6490 6480 6540
Wire Wire Line
	5330 6040 5330 6490
Wire Wire Line
	5330 6490 5730 6490
$Comp
L power:+3.3V #PWR?
U 1 1 604C422F
P 3600 5930
AR Path="/5EE620E3/604C422F" Ref="#PWR?"  Part="1" 
AR Path="/604C422F" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3600 5780 50  0001 C CNN
F 1 "+3.3V" H 3615 6103 50  0000 C CNN
F 2 "" H 3600 5930 50  0001 C CNN
F 3 "" H 3600 5930 50  0001 C CNN
	1    3600 5930
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 604C4235
P 5330 6040
AR Path="/5EE620E3/604C4235" Ref="#PWR?"  Part="1" 
AR Path="/604C4235" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 5330 5890 50  0001 C CNN
F 1 "+3.3V" H 5345 6213 50  0000 C CNN
F 2 "" H 5330 6040 50  0001 C CNN
F 3 "" H 5330 6040 50  0001 C CNN
	1    5330 6040
	1    0    0    -1  
$EndComp
Wire Wire Line
	5730 6090 5480 6090
Wire Wire Line
	5730 6190 5480 6190
Wire Wire Line
	5480 6290 5730 6290
Wire Wire Line
	5480 6390 5730 6390
Wire Wire Line
	6480 6090 6230 6090
Wire Wire Line
	6480 6190 6230 6190
Wire Wire Line
	6230 6290 6480 6290
Wire Wire Line
	6230 6390 6480 6390
Text Label 5480 6390 0    50   ~ 0
MISO_0
Text Label 5480 6290 0    50   ~ 0
SS_0
Text Label 5480 6190 0    50   ~ 0
SCLK_0
Text Label 5480 6090 0    50   ~ 0
MOSI_0
Text Label 6480 6390 2    50   ~ 0
MISO_1
Text Label 6480 6290 2    50   ~ 0
SS_1
Text Label 6480 6190 2    50   ~ 0
SCLK_1
Text Label 6480 6090 2    50   ~ 0
MOSI_1
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 604C424B
P 2830 7320
AR Path="/5EE620E3/604C424B" Ref="J?"  Part="1" 
AR Path="/604C424B" Ref="J3"  Part="1" 
F 0 "J3" H 2880 7737 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2880 7646 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2830 7320 50  0001 C CNN
F 3 "~" H 2830 7320 50  0001 C CNN
	1    2830 7320
	1    0    0    -1  
$EndComp
Wire Wire Line
	3130 7520 3380 7520
$Comp
L power:GND #PWR?
U 1 1 604C4252
P 3380 7570
AR Path="/5EE620E3/604C4252" Ref="#PWR?"  Part="1" 
AR Path="/604C4252" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 3380 7320 50  0001 C CNN
F 1 "GND" H 3480 7420 50  0000 C CNN
F 2 "" H 3380 7570 50  0001 C CNN
F 3 "" H 3380 7570 50  0001 C CNN
	1    3380 7570
	1    0    0    -1  
$EndComp
Wire Wire Line
	3380 7520 3380 7570
Wire Wire Line
	2230 7070 2230 7520
Wire Wire Line
	2230 7520 2630 7520
$Comp
L power:+3.3V #PWR?
U 1 1 604C425B
P 2230 7070
AR Path="/5EE620E3/604C425B" Ref="#PWR?"  Part="1" 
AR Path="/604C425B" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2230 6920 50  0001 C CNN
F 1 "+3.3V" H 2245 7243 50  0000 C CNN
F 2 "" H 2230 7070 50  0001 C CNN
F 3 "" H 2230 7070 50  0001 C CNN
	1    2230 7070
	1    0    0    -1  
$EndComp
Wire Wire Line
	2630 7120 2380 7120
Wire Wire Line
	2630 7220 2380 7220
Wire Wire Line
	2380 7320 2630 7320
Wire Wire Line
	2380 7420 2630 7420
Wire Wire Line
	3380 7120 3130 7120
Wire Wire Line
	3380 7220 3130 7220
Wire Wire Line
	3130 7320 3380 7320
Wire Wire Line
	3130 7420 3380 7420
Text Label 2380 7120 0    50   ~ 0
SDA_0
Text Label 3380 7120 2    50   ~ 0
SCL_0
Text Label 2380 7220 0    50   ~ 0
TX_1
Text Label 3380 7220 2    50   ~ 0
RX_1
Text Label 2380 7320 0    50   ~ 0
TX_LED
Text Label 3380 7320 2    50   ~ 0
RX_LED
Text Label 2380 7420 0    50   ~ 0
D8
Text Notes 2490 6550 0    50   ~ 0
Conn Analogico\n
Text Notes 3950 6840 0    50   ~ 0
Conn Digital 1\nSerie 0\n\n\n
Text Notes 5750 6920 0    50   ~ 0
Conn Digital 2\nSPI (0 + 1)\n\n
Text Notes 2610 7910 0    50   ~ 0
Conn Digital 3\nI2C + Serie 1\n\n\n
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 604C4282
P 4140 7360
AR Path="/5EE620E3/604C4282" Ref="J?"  Part="1" 
AR Path="/604C4282" Ref="J4"  Part="1" 
F 0 "J4" H 4058 7035 50  0000 C CNN
F 1 "Conn_01x02" H 4058 7126 50  0000 C CNN
F 2 "_CONN:WF142V-508-2" H 4140 7360 50  0001 C CNN
F 3 "~" H 4140 7360 50  0001 C CNN
	1    4140 7360
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 604C4288
P 5430 7420
AR Path="/5EE620E3/604C4288" Ref="J?"  Part="1" 
AR Path="/604C4288" Ref="J6"  Part="1" 
F 0 "J6" H 5348 7095 50  0000 C CNN
F 1 "Conn_01x02" H 5348 7186 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Latch_53253-0270_1x02_P2.00mm_Vertical" H 5430 7420 50  0001 C CNN
F 3 "~" H 5430 7420 50  0001 C CNN
	1    5430 7420
	-1   0    0    1   
$EndComp
Wire Wire Line
	4340 7260 4590 7260
Wire Wire Line
	4590 7260 4590 7210
Wire Wire Line
	5830 7320 5830 7220
Wire Wire Line
	5630 7420 5880 7420
Wire Wire Line
	5880 7420 5880 7470
$Comp
L power:GND #PWR?
U 1 1 604C42BB
P 5880 7470
AR Path="/5EE620E3/604C42BB" Ref="#PWR?"  Part="1" 
AR Path="/604C42BB" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 5880 7220 50  0001 C CNN
F 1 "GND" H 5980 7320 50  0000 C CNN
F 2 "" H 5880 7470 50  0001 C CNN
F 3 "" H 5880 7470 50  0001 C CNN
	1    5880 7470
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 604C42C1
P 4390 7410
AR Path="/5EE620E3/604C42C1" Ref="#PWR?"  Part="1" 
AR Path="/604C42C1" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 4390 7160 50  0001 C CNN
F 1 "GND" H 4490 7260 50  0000 C CNN
F 2 "" H 4390 7410 50  0001 C CNN
F 3 "" H 4390 7410 50  0001 C CNN
	1    4390 7410
	1    0    0    -1  
$EndComp
$Comp
L power:VPP #PWR012
U 1 1 604C4372
P 5830 7220
AR Path="/604C4372" Ref="#PWR012"  Part="1" 
AR Path="/5E776248/604C4372" Ref="#PWR?"  Part="1" 
AR Path="/5EE620E3/604C4372" Ref="#PWR?"  Part="1" 
F 0 "#PWR012" H 5830 7070 50  0001 C CNN
F 1 "VPP" H 5845 7393 50  0000 C CNN
F 2 "" H 5830 7220 50  0001 C CNN
F 3 "" H 5830 7220 50  0001 C CNN
	1    5830 7220
	1    0    0    -1  
$EndComp
Wire Wire Line
	4390 7360 4390 7410
Wire Wire Line
	4340 7360 4390 7360
$Comp
L power:VCC #PWR?
U 1 1 604C43F3
P 4590 7210
AR Path="/5EE620E3/604C43F3" Ref="#PWR?"  Part="1" 
AR Path="/604C43F3" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 4590 7060 50  0001 C CNN
F 1 "VCC" H 4607 7383 50  0000 C CNN
F 2 "" H 4590 7210 50  0001 C CNN
F 3 "" H 4590 7210 50  0001 C CNN
	1    4590 7210
	1    0    0    -1  
$EndComp
Text Label 3380 7420 2    50   ~ 0
D9
NoConn ~ 3340 6310
Text Label 1830 6750 0    50   ~ 0
SWCLK
Text Label 1830 6850 0    50   ~ 0
SWDIO
Text Label 1830 6550 0    50   ~ 0
RESET
Wire Wire Line
	5630 7320 5830 7320
$EndSCHEMATC
